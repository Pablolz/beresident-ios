//
//  beResidentUserIOSApp.swift
//  beResidentUserIOS
//
//  Created by Benny on 01/10/22.
//

import SwiftUI

@main
struct beResidentUserIOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
